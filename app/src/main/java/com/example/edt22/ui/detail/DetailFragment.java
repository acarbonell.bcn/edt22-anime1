package com.example.edt22.ui.detail;

import static com.example.edt22.LogInActivity.FILE_SHARED_NAME;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.edt22.databinding.FragmentDetailBinding;
import com.example.edt22.databinding.RowDataBinding;
import com.example.edt22.model.Anime;
import com.example.edt22.model.User;
import com.example.edt22.ui.list.ListFragment;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class DetailFragment extends Fragment {

    private FragmentDetailBinding binding;
    List<User> users = new ArrayList<>();


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        DetailViewModel detailViewModel =
                new ViewModelProvider(this).get(DetailViewModel.class);

        binding = FragmentDetailBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        //final TextView textView = binding.textDashboard;
        //detailViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        getAnime();


        for (int i = 0; i < users.size(); i++){
            Log.i("aa", "bbb");
            if (users.get(i).getUsername() == "user01"){
                binding.textNameProfile.setText(users.get(i).getUsername());
                Log.i("aa", "eee");
                //binding.textEmailProfile.setText(users.get(i).getUserid());
            }
        }

    }

    public void getAnime(){
        RequestQueue queue = Volley.newRequestQueue(requireContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                "https://m08anime.herokuapp.com/users/",
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("result");
                            for (int i = 0; i < response.getJSONArray("result").length(); i++) {
                                try {
                                    JSONObject animeObject = jsonArray.getJSONObject(i);

                                    User user = new User();
                                    user.setUsername(animeObject.getString("username"));
                                    user.setUserid(animeObject.getString("userid"));

                                    users.add(user);
                                    Toast.makeText(requireContext(), animeObject.getString("username"), Toast.LENGTH_SHORT).show();


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.i("aa", String.valueOf(users.size()));

                        for (int i = 0; i < users.size(); i++){
                            Log.i("aa", users.get(i).getUsername());
                            //binding.textNameProfile.setText(users.get(0).getUsername());

                            if (users.get(i).getUsername().equals("user01")){
                                binding.textNameProfile.setText(users.get(i).getUsername());
                                Log.i("aa", "ddd");
                                binding.textEmailProfile.setText(users.get(i).getUserid());
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(requireContext(), "Error", Toast.LENGTH_SHORT).show();
            }
        }){
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                SharedPreferences sharedPreferences = getContext().getSharedPreferences(FILE_SHARED_NAME,
                        Context.MODE_PRIVATE);
                String a = sharedPreferences.getString("encode", "patata");
                headers.put("Authorization", "Basic " + a);
                return headers;
            }
        };
        queue.add(jsonObjectRequest);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}