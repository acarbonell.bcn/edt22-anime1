package com.example.edt22.ui.favorite;

import static com.example.edt22.LogInActivity.FILE_SHARED_NAME;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.edt22.databinding.FragmentFavoriteBinding;
import com.example.edt22.databinding.RowDataBinding;
import com.example.edt22.model.Anime;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FavoriteFragment extends Fragment {

    private FragmentFavoriteBinding binding;

    List<Anime> animes = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        FavoriteViewModel favoriteViewModel =
                new ViewModelProvider(this).get(FavoriteViewModel.class);

        binding = FragmentFavoriteBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        return root;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getAnime();
    }

    public void getAnime(){
        RequestQueue queue = Volley.newRequestQueue(requireContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                "https://m08anime.herokuapp.com/users/favorites",
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("result");
                            for (int i = 0; i < response.getJSONArray("result").length(); i++) {
                                try {
                                    JSONObject animeObject = jsonArray.getJSONObject(i);
                                    JSONArray jsonArray1 = animeObject.getJSONArray("favorites");
                                    for (int j = 0; j < animeObject.getJSONArray("favorites").length(); j++) {
                                        try {
                                            JSONObject favObject = jsonArray1.getJSONObject(j);
                                            Anime  anime = new Anime();
                                            anime.setName(favObject.getString("name"));
                                            anime.setDescription(favObject.getString("description"));
                                            anime.setYear(favObject.getString("year"));
                                            anime.setType(favObject.getString("type"));
                                            anime.setImageurl(favObject.getString("imageurl"));
                                            animes.add(anime);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        MyAdapter2 myAdapter2;
                        binding.rViewAnimesFav.setAdapter(myAdapter2 = new MyAdapter2());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(requireContext(), "Error", Toast.LENGTH_SHORT).show();
            }
        }){
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                SharedPreferences sharedPreferences = getContext().getSharedPreferences(FILE_SHARED_NAME,
                        Context.MODE_PRIVATE);
                String a = sharedPreferences.getString("encode", "patata");
                headers.put("Authorization", "Basic "+ a);
                return headers;
            }
        };
        queue.add(jsonObjectRequest);
    }

    class MyAdapter2 extends RecyclerView.Adapter<MyAdapter2.MyViewHolder2>{


        @NonNull
        @Override
        public MyViewHolder2 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder2(RowDataBinding.inflate(getLayoutInflater(), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder2 holder, int position) {
            Anime anime = animes.get(position);
            Picasso.get().load(anime.getImageurl()).fit().centerCrop().into(holder.binding.imgAnime);
            holder.binding.textTitle.setText(anime.getName());
            holder.binding.textDesc.setText(anime.getDescription());
            holder.binding.textYear.setText(anime.getYear());
            holder.binding.textType.setText(anime.getType());
            /*Usser user = users.get(position);
            holder.binding.textTitle.setText(user.getUsername());
            holder.binding.textDesc.setText(user.getUserid());

             */
        }

        @Override
        public int getItemCount() {
            return animes.size();
        }

        public class MyViewHolder2 extends RecyclerView.ViewHolder {
            RowDataBinding binding;
            public MyViewHolder2(RowDataBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}