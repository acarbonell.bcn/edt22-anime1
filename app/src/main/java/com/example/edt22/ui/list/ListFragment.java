package com.example.edt22.ui.list;

import static com.example.edt22.LogInActivity.FILE_SHARED_NAME;
import static com.example.edt22.MainActivity.enc;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.edt22.MainActivity;
import com.example.edt22.databinding.FragmentListBinding;
import com.example.edt22.databinding.RowDataBinding;
import com.example.edt22.model.Anime;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListFragment extends Fragment {

    private FragmentListBinding binding;

    List<Anime> animes = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        ListViewModel listViewModel =
                new ViewModelProvider(this).get(ListViewModel.class);

        binding = FragmentListBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        return root;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getAnime();
    }

    public void getAnime(){
        RequestQueue queue = Volley.newRequestQueue(requireContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                "https://m08anime.herokuapp.com/anime/",
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("result");
                            for (int i = 0; i < response.getJSONArray("result").length(); i++) {
                                try {
                                    JSONObject animeObject = jsonArray.getJSONObject(i);
                                    Anime anime = new Anime();
                                    anime.setName(animeObject.getString("name"));
                                    anime.setDescription(animeObject.getString("description"));
                                    anime.setYear(animeObject.getString("year"));
                                    anime.setType(animeObject.getString("type"));
                                    anime.setImageurl(animeObject.getString("imageurl"));
                                    animes.add(anime);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        MyAdapter myAdapter;
                        binding.rViewAnimes.setAdapter(myAdapter = new MyAdapter());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(requireContext(), "Error", Toast.LENGTH_SHORT).show();
            }
        }){
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                SharedPreferences sharedPreferences = getContext().getSharedPreferences(FILE_SHARED_NAME,
                        Context.MODE_PRIVATE);
                String a = sharedPreferences.getString("encode", "patata");
                headers.put("Authorization", "Basic " + a);
                return headers;
            }
        };
        queue.add(jsonObjectRequest);
    }

    class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(RowDataBinding.inflate(getLayoutInflater(), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            Anime anime = animes.get(position);
            Picasso.get().load(anime.getImageurl()).fit().centerCrop().into(holder.binding.imgAnime);
            //Glide.with(requireContext()).load(anime.getImageurl()).into(holder.binding.imgAnime);
            holder.binding.textTitle.setText(anime.getName());
            holder.binding.textDesc.setText(anime.getDescription());
            holder.binding.textYear.setText(anime.getYear());
            holder.binding.textType.setText(anime.getType());
        }

        @Override
        public int getItemCount() {
            return animes.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            RowDataBinding binding;
            public MyViewHolder(RowDataBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}