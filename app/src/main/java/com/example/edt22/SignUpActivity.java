package com.example.edt22;

import static com.example.edt22.LogInActivity.FILE_SHARED_NAME;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.edt22.databinding.ActivitySignUpBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {

    private ActivitySignUpBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((binding = ActivitySignUpBinding.inflate(getLayoutInflater())).getRoot());

        binding.btnLogIn.setOnClickListener(v -> {
            Intent intent = new Intent(SignUpActivity.this, LogInActivity.class);
            startActivity(intent);
        });

        binding.btnSignUp.setOnClickListener(v -> {
            Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
            newUser();
            startActivity(intent);
        });
    }

    public void newUser(){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("username", "user09");
            jsonBody.put("password:", "pass");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String mReuqestBody = jsonBody.toString();

        JsonObjectRequest stringRequest = new JsonObjectRequest(
                    Request.Method.POST,
                    "https://m08anime.herokuapp.com/users/register",
                    null,
                new Response.Listener<JSONObject>(){
                    @Override    public void onResponse(JSONObject response) {
                        Log.i("Response",String.valueOf(response));
                    }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("LOG_RESPONSE", error.toString());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    return headers;
                }

            @Override
            public byte[] getBody() {
                try {
                    return  mReuqestBody == null ? null : mReuqestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mReuqestBody, "utf-8");
                    return null;
                }
            }
        };
            requestQueue.add(stringRequest);

    }

}