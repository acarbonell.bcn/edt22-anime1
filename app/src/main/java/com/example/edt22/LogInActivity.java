package com.example.edt22;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import com.example.edt22.databinding.ActivityLogInBinding;

import java.util.Base64;

public class LogInActivity extends AppCompatActivity {

    public static final String FILE_SHARED_NAME = "MySharedFile";


    private ActivityLogInBinding binding;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((binding = ActivityLogInBinding.inflate(getLayoutInflater())).getRoot());



        binding.btnLogIn.setOnClickListener(v -> {
            String user = binding.textEmail.getText().toString();
            String pass = binding.textPassword.getText().toString();
            String unio = user + ":" + pass;
            String code = Base64.getEncoder().encodeToString(unio.getBytes());
            //Toast.makeText(this, code, Toast.LENGTH_SHORT).show();
            Intent i = new Intent(LogInActivity.this, MainActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("enc", code);
            i.putExtras(bundle);
            SharedPreferences sharPref =  getSharedPreferences(FILE_SHARED_NAME, MODE_PRIVATE);
            SharedPreferences.Editor editor  = sharPref.edit();
            editor.putString("username", user);
            editor.putString("password", pass);
            editor.putString("encode",code);
            editor.apply();
            startActivity(i);
        });

        binding.btnSignUp.setOnClickListener(v -> {
            Intent intent = new Intent(LogInActivity.this, SignUpActivity.class);
            startActivity(intent);
        });
    }
}