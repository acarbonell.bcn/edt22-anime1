package com.example.edt22;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.edt22.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    //public static String EXTRA_TEXT = "com.example.edt22.EXTRA_TEXT";

    private ActivityMainBinding binding;

    public static String enc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_list, R.id.navigation_details, R.id.navigation_favorite)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_activity_main);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, navController);

        //Intent i = getIntent();
        //enc = i.getStringExtra("enc");
        //Bundle bundle = getIntent().getExtras();
        //String name = bundle.getString("enc");
        //final String enc = bundle.getString("enc");
        //Toast.makeText(this, enc, Toast.LENGTH_SHORT).show();
        //Bundle bundle = new Bundle();
        //bundle.putString("enc", enc);
        //navController.navigate(R.id.navigation_list, bundle);

    }




}